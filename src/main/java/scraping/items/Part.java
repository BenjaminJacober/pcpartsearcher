package scraping.items;

import org.javamoney.moneta.FastMoney;

import java.util.Objects;

// Model
public class Part implements Comparable<Part> {

    private String countryCode;
    private String shopLink;
    private String name;
    private FastMoney price;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FastMoney getPrice() {
        return price;
    }

    public void setPrice(FastMoney price) {
        this.price = price;
    }

    public String getInformation() {
        return "- ScrapingPart -" + "\n" +
                "name: " + name + "\n" +
                "link: " + shopLink + "\n" +
                "price: " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Part part = (Part) o;
        return Objects.equals(countryCode, part.countryCode) &&
                Objects.equals(shopLink, part.shopLink) &&
                Objects.equals(name, part.name) &&
                Objects.equals(price, part.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryCode, shopLink, name, price);
    }

    @Override
    public int compareTo(Part o) {
        // Its better if an item is cheaper. 1 For more expensive, 0 for cheaper.
        return this.price.isLessThan(o.getPrice())
                ? 1
                : 0;
    }
}
