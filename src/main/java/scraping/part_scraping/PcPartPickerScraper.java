package scraping.part_scraping;

import org.javamoney.moneta.FastMoney;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import scraping.finance_information_scraping.TaxScraper;
import scraping.items.Part;

import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author benij
 */
public class PcPartPickerScraper extends PartScraper {

    private String countryCode;
    private static final CurrencyConversion CONVERSION_TO_CHF = MonetaryConversions.getConversion("CHF");

    /**
     * @param webDriver the web driver
     */
    public PcPartPickerScraper(WebDriver webDriver) {
        super(webDriver, "products");
    }

    // Todo: pass List of Selectors with type and query or id.
    // Todo: and SelectorType enum.
    // Todo: Guess type based on string.
    public Part getPart(String partIdOrQuery, String countryCode) {
        this.countryCode = countryCode;

        partLink = findPartLink(partIdOrQuery);
        loadHTML();

        Part part = new Part();
        part.setName(findName());
        part.setPrice(findPrice());
        part.setShopLink(findShopLink());
        part.setCountryCode(countryCode);
        return part;
    }

    @Override
    protected String findPartLinkById(String productId) {
        return "https://" + countryCode + ".pcpartpicker.com" + productId;
    }

    @Override
    protected String findPartLinkByQuery(String productQuery) {
        String queryLink = findPartLinkById(productQuery);

        webDriver.get(queryLink);
        setMerchants();

        // Todo doesn't belong here
        // For some reason an alert with the content "An unknown error occurred" shows up at this point for the french PcPartPicker site.
        if (isAlertPresent()) {
            webDriver.switchTo().alert().accept();
        }

        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.id("category_content")));
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.className("tr__product")));

        Document document = Jsoup.parse(webDriver.getPageSource());

        resetPage();

        Element firstProduct = document.getElementsByClass("tr__product").get(0);
        Element firstProductName = firstProduct.getElementsByClass("td__name").get(0);

        return findPartLinkById(firstProductName.getElementsByTag("a").get(0).attr("href"));
    }

    /**
     * Helper method to set amazon as the only viable merchant on PcPartPicker site.
     */
    private void setMerchants() {
        String expandMerchantsButtonXPath = "/html/body/section/div/div[2]/section/div/div[1]/div/div[2]/div/section/div[2]/h3/span";
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(expandMerchantsButtonXPath)));
        webDriver.findElement(By.xpath(expandMerchantsButtonXPath)).click();

        String allMerchantsPath = "/html/body/section/div/div[2]/section/div/div[1]/div/div[2]/div/section/div[2]/ul";
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(allMerchantsPath)));

        WebElement allMerchantsUl = webDriver.findElement(By.xpath(allMerchantsPath));
        WebElement currentLi = allMerchantsUl.findElement(By.tagName("li"));
        int counter = 0;
        while (true) {
            WebElement currentLabel = currentLi.findElement(By.tagName("label"));
            if (currentLabel.getText().contains("Amazon")) {
                WebElement checkBox = currentLi.findElement(By.tagName("input"));
                if (checkBox.getAttribute("checked") == null) {
                    currentLabel.click();
                }
                break;
            }
            counter += 1;
            currentLi = currentLi.findElement(By.xpath("/html/body/section/div/div[2]/section/div/div[1]/div/div[2]/div/section/div[2]/ul/li[" + counter + "]/following-sibling::li"));
        }
    }

    @Override
    public void loadHTML() {
        webDriver.get(partLink);
        jsoup = Jsoup.parse(webDriver.getPageSource());
    }

    @Override
    protected String findName() {
        return jsoup.getElementsByClass("pageTitle").get(0).text();
    }

    @Override
    protected String findShopLink() {
        Elements allLinks = jsoup.getElementsByTag("a");

        return findPartLink(allLinks.stream().map(element -> element.attr("href"))
                .filter(href -> href.contains("amazon"))
                .findFirst()
                .orElseGet(null));
    }

    @Override
    protected FastMoney findPrice() {
        Elements td__finalPrices = jsoup.getElementsByClass("td__finalPrice");
        // as means all a tags. (a's)
        List<Element> as = td__finalPrices.stream()
                .map(element -> element.getElementsByTag("a").get(0))
                .collect(Collectors.toList());

        for (Element element : as) {
            String href = element.attr("href");
            if (href.contains("amazon")) {
                String currencyCode;

                // Workaround since we use UK for the PcPartPicker site and javaMoney uses GB.
                if ("UK".equals(countryCode)) {
                    currencyCode = "GBP";
                } else {
                    currencyCode = Currency.getInstance(new Locale("", countryCode)).getCurrencyCode();
                }

                FastMoney price = FastMoney.of(Double.parseDouble(element.text().replaceAll(TEXT_TO_FLOAT_REGEX, "")), currencyCode);
                price = adjustTaxes(price);
                price = convertCurrency(price);
                return price;
            }
        }

        return null;
    }

    /**
     * Adjust taxes by removing foreign tax and adding swiss tax.
     */
    private FastMoney adjustTaxes(FastMoney price) {
        // Removes taxes from current region
        price = price.divide(TaxScraper.TAXLIST.get(countryCode) + 100);
        // Adds swiss tax
        price = price.multiply(TaxScraper.TAXLIST.get("CH") + 100);
        return price;
    }

    private FastMoney convertCurrency(FastMoney price) {
        return price.with(CONVERSION_TO_CHF);
    }

}
