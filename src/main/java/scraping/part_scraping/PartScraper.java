package scraping.part_scraping;

import org.javamoney.moneta.FastMoney;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author benij
 */
// Controller
public abstract class PartScraper {

    protected static final String TEXT_TO_FLOAT_REGEX = "[^0-9.]";
    protected WebDriver webDriver;
    protected WebDriverWait webDriverWait;
    protected Document jsoup;
    protected String queryIdentifier;
    protected String partLink;

    /**
     * @param webDriver       the web driver
     * @param queryIdentifier string that's supposed to be in all queries of a site to identify them.
     */
    public PartScraper(WebDriver webDriver, String queryIdentifier) {
        this.webDriver = webDriver;
        this.queryIdentifier = queryIdentifier;
        webDriverWait = new WebDriverWait(webDriver, 60);
    }

    /**
     * Checks the webDriver for an alert pop up.
     *
     * @return true if alert is present, false if there is no alert.
     */
    protected boolean isAlertPresent() {
        return ExpectedConditions.alertIsPresent().apply(webDriver) != null;
    }

    /**
     * Organic wait until. We'll get blocked if were too fast.
     *
     * @param expectedCondition the expected condition
     */
    protected void organicWaitUntil(ExpectedCondition expectedCondition) {
        webDriverWait.until(expectedCondition);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    protected void resetPage() {
        webDriver.get("about:blank");
    }

    /**
     * Identifies if the productIdOrQuery string is a product or query and gets link based on that.
     *
     * @param productIdOrQuery the product id or query
     */
    protected String findPartLink(String productIdOrQuery) {
        return productIdOrQuery.contains(queryIdentifier)
                ? findPartLinkByQuery(productIdOrQuery)
                : findPartLinkById(productIdOrQuery);

    }

    protected abstract String findPartLinkById(String productId);

    protected abstract String findPartLinkByQuery(String productQuery);

//    public abstract Part getPart();

    protected abstract String findName();

    protected abstract String findShopLink();

    protected abstract FastMoney findPrice();

    protected abstract void loadHTML();

    // todo: throw UnsupportedOperationException if not implemented
//    protected abstract void setStockAvailability();
//    protected abstract void setMerchantAvailability();

    /**
     * Only for testing purposes.
     */
    public void setJsoup(Document jsoup) {
        this.jsoup = jsoup;
    }
}