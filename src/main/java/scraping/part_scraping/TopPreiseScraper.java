package scraping.part_scraping;

import org.javamoney.moneta.FastMoney;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import scraping.items.Part;

import java.util.Currency;
import java.util.Locale;

/**
 * @author benij
 */
public class TopPreiseScraper extends PartScraper {

    /**
     * Instantiates a new TopPreise part.
     *
     * @param webDriver the web driver
     */
    public TopPreiseScraper(WebDriver webDriver) {
        super(webDriver, "produktsuche");
    }

    // Todo: pass List of Selectors with type and query or id.
    // Todo: Guess type based on string.
    public Part getPart(String partIdOrQuery) {
        // todo: should findPartLink set the partLink itself or should it be done here?
        partLink = findPartLink(partIdOrQuery);
        loadHTML();

        Part part = new Part();
        part.setName(findName());
        part.setPrice(findPrice());
        part.setShopLink(findShopLink());
        part.setCountryCode("CH");
        return part;
    }

    @Override
    protected String findPartLinkById(String productId) {
        return "https://www.toppreise.ch" + productId;
    }

    @Override
    protected String findPartLinkByQuery(String productQuery) {
        String queryLink = findPartLinkById(productQuery);

        webDriver.get(queryLink);

        // Maybe there needs to be another setStockAvailability here.

        // Waits until filters have been loaded IMPORTANT, this will fuck up any search that doesn't contain a filter.
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='f_item_active_filter ngf_active_filter pt-2 pl-2']")));

        // Waits until at least 1 part has been loaded
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='Plugin_Product f_clickable mixedBrowsingList container mixedList']")));

        jsoup = Jsoup.parse(webDriver.getPageSource());
        resetPage();

        Element firstProduct = jsoup.getElementsByClass("Plugin_Product f_clickable mixedBrowsingList container mixedList").get(0);

        return findPartLinkById(firstProduct.attr("data-link"));
    }

    @Override
    public void loadHTML() {
        webDriver.get(partLink);

        setStockAvailability();

        // We have to use div[...] instead of byClass, because byClass doesn't work with class names that have spaces in them.
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='Plugin_Offer f_clickable col-12 active productDetailsOfferList ']")));

        jsoup = Jsoup.parse(webDriver.getPageSource());
        resetPage();
    }

    private void setStockAvailability() {
        String availabilityButtonXPath = "/html/body/div[1]/div[3]/div[2]/div[6]/div[2]/div[1]/div[2]/div/div/div[1]/div[2]/div[1]/div[2]/div/div/div[1]/span[1]";
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(availabilityButtonXPath)));
        webDriver.findElement(By.xpath(availabilityButtonXPath)).click();

        String oneWeekAvailabilityButtonXPath = "/html/body/div[1]/div[3]/div[2]/div[6]/div[2]/div[1]/div[2]/div/div/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/ul/li[2]";
        organicWaitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(oneWeekAvailabilityButtonXPath)));
        webDriver.findElement(By.xpath(oneWeekAvailabilityButtonXPath)).click();
    }

    @Override
    protected String findName() {
        return jsoup.getElementsByClass("product-name m-0 d-none d-lg-block").get(0).text();
    }

    @Override
    protected String findShopLink() {
        return findPartLinkById(jsoup.getElementsByClass("offer-name").get(0).attr("href"));
    }

    @Override
    protected FastMoney findPrice() {
        Element partList = jsoup.getElementsByClass("Plugin_PriceComparisonOfferList f_Plugin_PriceComparisonOfferList  boldShippingPrice  ").get(0);
        Element firstPart = partList.getElementsByClass("Plugin_Offer f_clickable col-12 active productDetailsOfferList ").get(0);
        Element priceContainer = firstPart.getElementsByClass("Plugin_PriceInformation price_information_offer").get(0);
        Element priceContainerShippingPrice = priceContainer.getElementsByClass("priceContainer shippingPrice ").get(0);
        Element cheapestOffer = priceContainerShippingPrice.getElementsByClass("Plugin_HashedPrice").get(0);
        String currencyCode = Currency.getInstance(new Locale("", "CH")).getCurrencyCode();
        return FastMoney.of(Double.parseDouble(cheapestOffer.getElementsByClass("Plugin_Price").get(0).text().replaceAll(TEXT_TO_FLOAT_REGEX, "")), currencyCode);
        // We don't have to adjustTaxes or convertCurrency, as TopPreiseItems prices are always with swiss taxes and in CHF.
    }
}