package scraping.finance_information_scraping;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * The type Tax scraper.
 *
 * @author benij
 */
// Todo: replace class with tax API.
// Todo: should this be Singleton?
public class TaxScraper {

    private static final String URL = "https://www.die-mehrwertsteuer.de/de/uebersicht-steuersaetze.html";
    public static final HashMap<String, Double> TAXLIST = findFinanceInformation();

    /**
     * Goes to URL website and loads a bootstrap table.
     *
     * @return HashMap with countries and their tax rates.
     */
    private static HashMap<String, Double> findFinanceInformation() {
        Document jsoup = setupJsoup();

        Elements allCountries = jsoup.getElementsByTag("tbody").get(0).getElementsByTag("tr");

        HashMap<String, Double> result = (HashMap<String, Double>) allCountries.stream()
                .collect(Collectors.toMap(
                        country -> country.getElementsByIndexEquals(1).text(),
                        country -> Double.parseDouble(country.getElementsByTag("td").get(2).text()
                                .replace("%", "").replace(",", ".")
                        )));

        // We have to replace GB with UK, which is the standard.
        result.put("UK", result.remove("GB"));
        return result;
    }

    /**
     * @return Document with site DOM or null if it couldn't be loaded.
     */
    private static Document setupJsoup() {
        try {
            return Jsoup.connect(URL).get();
        } catch (IOException e) {
            // todo: Use Logger
            System.out.println("Couldn't get HTML of: " + URL);
            return null;
        }
    }

}
