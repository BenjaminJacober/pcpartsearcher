package scraping.PartUtility;

import scraping.items.Part;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author benij
 */
public class PartComparer {

    private final SortedSet<Part> parts = new TreeSet<>();

    public void addPart(Part part) {
        parts.add(part);
    }

    public void addAllPart(Part... additionalParts) {
        parts.addAll(Arrays.asList(additionalParts));
    }

    public Part getCheapestPart() {
        return parts.last();
    }

}
