import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import scraping.PartUtility.PartComparer;
import scraping.part_scraping.PcPartPickerScraper;
import scraping.part_scraping.TopPreiseScraper;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        // ----------------------------------------------------------
        // Settings
        // todo: create firefox profile and properties file

        System.setProperty("webdriver.gecko.driver", "D:\\Programmier-Zeugs\\Programming-Software\\geckodriver-v0.27.0-win64\\geckodriver.exe");

        // Removing unnecessary JavaScript errors/warnings.
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");

        // FirefoxProfile profile = new FirefoxProfile("");
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("dom.webdriver.enabled", false);
        profile.setPreference("useAutomationExtension", false);
        profile.setPreference("permissions.default.image", 2);
        profile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so", false);
        profile.setPreference("webgl.disabled", true);
        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);
        // options.addArguments("--headless");

        WebDriver webDriver = new FirefoxDriver(options);

        webDriver.manage().window().maximize();
        // Sets max loading time for a website.
        webDriver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);

        // ----------------------------------------------------------
        // Parts
        // Todo: multithreading for ECB API

        TopPreiseScraper tpScraper = new TopPreiseScraper(webDriver);
        PcPartPickerScraper pppScraper = new PcPartPickerScraper(webDriver);

        PartComparer CPU_Intel_I5_10400F = new PartComparer();
        CPU_Intel_I5_10400F.addAllPart(
                tpScraper.getPart("/preisvergleich/Prozessoren/INTEL-Core-i5-10400F-Comet-Lake-S-6x-2-9GHz-Sockel-BX8070110400F-p610655"),
                pppScraper.getPart("/product/vrhmP6/intel-core-i5-10400f-29-ghz-6-core-processor-bx8070110400f", "DE"),
                pppScraper.getPart("/product/vrhmP6/intel-core-i5-10400f-29-ghz-6-core-processor-bx8070110400f", "FR"),
                pppScraper.getPart("/product/vrhmP6/intel-core-i5-10400f-29-ghz-6-core-processor-bx8070110400f", "IT"),
                pppScraper.getPart("/product/vrhmP6/intel-core-i5-10400f-29-ghz-6-core-processor-bx8070110400f", "UK")

        );

//        PartComparer CPU_AMD_Ryzen5_3600 = new PartComparer();
//        CPU_AMD_Ryzen5_3600.addAllPart(
//                tpScraper.getPart("/preisvergleich/Prozessoren/AMD-Ryzen-5-3600-Matisse-6x-3-6GHz-Sockel-100-100000031BOX-p563438"),
//                pppScraper.getPart("/product/9nm323/amd-ryzen-5-3600-36-thz-6-core-processor-100-100000031box", "DE"),
//                pppScraper.getPart("/product/9nm323/amd-ryzen-5-3600-36-thz-6-core-processor-100-100000031box", "FR"),
//                pppScraper.getPart("/product/9nm323/amd-ryzen-5-3600-36-thz-6-core-processor-100-100000031box", "IT"),
//                pppScraper.getPart("/product/9nm323/amd-ryzen-5-3600-36-thz-6-core-processor-100-100000031box", "UK")
//
//        );
//
//        PartComparer MB_AM4 = new PartComparer();
//        MB_AM4.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Mainboards/Mainboards-c140#oi~ongff605x18%3asv67453%3a%3aongff639x18%3asv8414.sv8428%3a%3aongff607x18%3asv49814%3a%3afngff608x18%3afv56358%3a%3afngff609x18%3afv8420%3a%3afngff1378x18%3afg803%3a%3arngff648x18%3afv8424%3a%3arngff649x18%3afv8408%2bs~pa%2ba~3%2be~1"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&s=33&sort=price&xcx=0&qq=1", "DE"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&s=33&sort=price&xcx=0&qq=1", "FR"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&s=33&sort=price&xcx=0&qq=1", "IT"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&s=33&sort=price&xcx=0&qq=1", "UK")
//
//        );
//
//        PartComparer MB_LGA1200 = new PartComparer();
//        MB_LGA1200.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Mainboards/Mainboards-c140#oi~ongff605x18%3asv83442%3a%3aongff639x18%3asv8414.sv8428%3a%3aongff607x18%3asv49814%3a%3afngff608x18%3afv56358%3a%3afngff609x18%3afv8420%3a%3afngff1378x18%3afg803%3a%3arngff648x18%3afv8424%3a%3arngff649x18%3afv8408%2bs~pa%2ba~3%2be~1"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&sort=price&xcx=0&s=39", "DE"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&sort=price&xcx=0&s=39", "FR"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&sort=price&xcx=0&s=39", "IT"),
//                pppScraper.getPart("/products/motherboard/#L=4&N=2,16&D=17179869184,2147483648000&f=2,7&sort=price&xcx=0&s=39", "UK")
//
//        );
//
//        PartComparer RAM_3200Plus = new PartComparer();
//        RAM_3200Plus.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Arbeitsspeicher-c147#oi~ongff815x25%3asv49586%3a%3arngff818x25%3afv14314%3a%3arngff817x25%3afv13744%3a%3arngff816x25%3afv14311%3a%3arngff821x25%3afv52062%2bs~pa"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=3200,5100&Z=16384002&sort=price&page=1", "DE"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=3200,5100&Z=16384002&sort=price&page=1", "FR"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=3200,5100&Z=16384002&sort=price&page=1", "IT"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=3200,5100&Z=16384002&sort=price&page=1", "UK")
//
//        );
//
//        PartComparer RAM_2933Plus = new PartComparer();
//        RAM_2933Plus.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Arbeitsspeicher-c147#oi~ongff815x25%3asv49586%3a%3arngff818x25%3afv14314%3a%3arngff817x25%3afv13744%3a%3arngff816x25%3afv14311%3a%3arngff821x25%3afv69897%2bs~pa"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=2933,5100&Z=16384002&sort=price&page=1", "DE"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=2933,5100&Z=16384002&sort=price&page=1", "FR"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=2933,5100&Z=16384002&sort=price&page=1", "IT"),
//                pppScraper.getPart("/products/memory/#xcx=0&t=14&U=4&S=2933,5100&Z=16384002&sort=price&page=1", "UK")
//
//        );
//
//        PartComparer STORAGE_SSD_2TBPlus = new PartComparer();
//        STORAGE_SSD_2TBPlus.addAllPart(
//                tpScraper.getPart("/preisvergleich/Solid-State-Drives-SSD/KINGSTON-A2000-SSD-M-2-1TB-SA2000M8-1000G-p567302"),
//                pppScraper.getPart("/product/9vWBD3/kingston-a2000-1-tb-m2-2280-nvme-solid-state-drive-sa2000m81000g", "DE"),
//                pppScraper.getPart("/product/9vWBD3/kingston-a2000-1-tb-m2-2280-nvme-solid-state-drive-sa2000m81000g", "FR"),
//                pppScraper.getPart("/product/9vWBD3/kingston-a2000-1-tb-m2-2280-nvme-solid-state-drive-sa2000m81000g", "IT"),
//                pppScraper.getPart("/product/9vWBD3/kingston-a2000-1-tb-m2-2280-nvme-solid-state-drive-sa2000m81000g", "UK")
//
//        );
//
//        PartComparer GPU_NVIDIA_2060S_or_AMD_RX5700XT = new PartComparer();
//        GPU_NVIDIA_2060S_or_AMD_RX5700XT.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Grafikkarten-Zubehoer/Grafikkarten-c37#oi~ongff417x14%3asv78723.sv78697%2bs~pa%2ba~3%2be~1"),
//                pppScraper.getPart("/products/video-card/#c=446,444&L=229000000,403000000&sort=price&page=1", "DE"),
//                pppScraper.getPart("/products/video-card/#c=446,444&L=229000000,403000000&sort=price&page=1", "FR"),
//                pppScraper.getPart("/products/video-card/#c=446,444&L=229000000,403000000&sort=price&page=1", "IT"),
//                pppScraper.getPart("/products/video-card/#c=446,444&L=229000000,403000000&sort=price&page=1", "UK")
//
//        );
//
//        PartComparer PSU_450_BronzePlus = new PartComparer();
//        PSU_450_BronzePlus.addAllPart(
//                tpScraper.getPart("/produktsuche/Computer-Zubehoer/PC-Komponenten/Gehaeuse-Netzteile/PC-Netzteile-c57#oi~rngff903x29%3afv16577%3a%3aongff1177x29%3asv33949.sv33951.sv33956.sv33955.sv43847%2bs~pa%2ba~3%2be~1"),
//                pppScraper.getPart("/products/power-supply/#e=6,5,4,3,2&A=450000000000,2000000000000&sort=price&page=1", "DE"),
//                pppScraper.getPart("/products/power-supply/#e=6,5,4,3,2&A=450000000000,2000000000000&sort=price&page=1", "FR"),
//                pppScraper.getPart("/products/power-supply/#e=6,5,4,3,2&A=450000000000,2000000000000&sort=price&page=1", "IT"),
//                pppScraper.getPart("/products/power-supply/#e=6,5,4,3,2&A=450000000000,2000000000000&sort=price&page=1", "UK")
//
//        );

        System.out.println(CPU_Intel_I5_10400F.getCheapestPart().getInformation());
//        System.out.println(CPU_AMD_Ryzen5_3600.getCheapestPart().getInformation());
//        System.out.println(MB_AM4.getCheapestPart().getInformation());
//        System.out.println(MB_LGA1200.getCheapestPart().getInformation());
//        System.out.println(RAM_3200Plus.getCheapestPart().getInformation());
//        System.out.println(RAM_2933Plus.getCheapestPart().getInformation());
//        System.out.println(STORAGE_SSD_2TBPlus.getCheapestPart().getInformation());
//        System.out.println(GPU_NVIDIA_2060S_or_AMD_RX5700XT.getCheapestPart().getInformation());
//        System.out.println(PSU_450_BronzePlus.getCheapestPart().getInformation());

        webDriver.quit();

    }
}
